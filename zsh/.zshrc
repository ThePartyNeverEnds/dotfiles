export PATH="/usr/local/sbin:$PATH"
export TERM="screen-256color"
export VIMRUNTIME=/usr/local/Cellar/neovim/0.5.0/share/nvim/runtime
export DOTFILES=$HOME/dotfiles
export STOW_FOLDERS="nvim,zsh,tmux,input,kitty,alacritty"
export PROJECTS=$HOME/Projects
export EDITOR=nvim

# ZSH PROMPT
NEWLINE=$'\n'
#PROMPT="%F{green}%~${NEWLINE}%F{magenta}λ%f "
PROMPT="%F{green}%~%F{magenta} ϟ%f "

# NAVIGATION EASE
alias b='cd ..'
alias dc='cd'
alias cdc='cd'
alias c='clear'
alias clera='clear'
alias celar='clear'
alias clearls='clear'
alias sl='ls -ltG'
alias ls='ls -lG'
alias sl=ls
alias q='exit'
alias serve='python3 -m http.serve'
alias fzfi='rg --files --hidden --follow --no-ignore-vcs -g "!{node_modules,.git}" | fzf'
alias ff='nvim $(fzfi)'

# MERCURIAL
#alias log='hg log -G'
#alias st='hg status'
#alias cm='hg add && hg commit -m'

# GIT
alias cm='git add . && git commit -am'
alias st='git status'
alias co='git checkout'
alias pullom='git pull origin master'
alias br='git branch';
alias log="git log --first-parent --graph --pretty=format:'%C(yellow)%h%Creset -%C(auto)%d%Creset %s %C(cyan)(%cr) %Cresetby %C(bold blue)%aN%Creset' --abbrev-commit --date=relative --branches"

# WWW
alias brave='{read -r arr; open -a "Brave Browser" "${arr}"} <<<'
alias browser='{read -r arr; brave ${arr} } <<<'
alias google='{read -r arr; browser "https://google.com/search?q=${arr}";} <<<'
alias gclone='cd ${PROJECTS} git clone https://www.github.com/'

# EDITOR
alias vim="nvim"
alias vi="nvim"
alias te="open -a TextEdit.app"

# DOTFILE MANAGEMENT
alias ss='source ~/.zshrc'
alias ac='nvim ~/.config/alacritty/alacritty.yml'
alias kc='nvim ~/.config/kitty/kitty.conf'
alias bc='nvim ~/.bashrc'
alias zc='nvim ~/.zshrc'
alias tc='nvim ~/.config/tmux/tmux.conf'
alias ic='nvim ~/.inputrc'
alias vc='nvim ~/.config/nvim/init.lua'
alias nvc='cd ~/dotfiles/nvim/.config/nvim/'
installDotfiles() {
    pushd $DOTFILES
    sh scripts/install.sh
    popd
}
commitDotfiles() { # credit: Michael Paulson @ Netflix
    pushd $DOTFILES
    git add .
    git commit -m "A mESSAGE fROM tHE pAST"
    git push origin master
    popd
}
removeDotfiles() {
    pushd $DOTFILES
    sh scripts/clean.sh
    popd
}
