#!/usr/bin/env zsh

echo "Hey there! Thx for giving my configuration a spin."
echo "-- WARNING --"
echo "This will irrevocably ERASE your current configuration"
echo "Still wanna proceed? (y/n)"
read CONTINUE
if [ $CONTINUE = "y" ]
then
    echo "OK, proceeding with installation"
    source ~/dotfiles/zsh/.zshrc
    sh ~/dotfiles/install.sh
    source ~/.zshrc
    echo "-> RESTART YOUR TERMINAL!"
elif [ $CONTINUE = "n" ]
then
    echo "Sorry to see you go!"
else
    echo "Couldn't understand your input...are you stupid?"
fi
