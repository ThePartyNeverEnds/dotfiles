vim.cmd[[
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim' " vim-plug not installed? then install it
if empty(glob(data_dir . '/autoload/plug.vim'))
	silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
]]
local function plug(path, config)
    vim.validate {
        path = {path, 's'},
        config = {config, vim.tbl_islist, 'an array of packages'},
    }
    vim.fn["plug#begin"](path)

    for _, v in ipairs(config) do
        if type(v) == "string" then
            vim.fn["plug#"](v)
        elseif type(v) == "table" then
            local p = v[1]
            assert(p, "Must specify package as first index.")
            v[1] = nil
            vim.fn["plug#"](p, v)
            v[1] = p
        end
    end
    vim.fn["plug#end"]()
end

plug(tostring(os.getenv("HOME")) .. "/.vim/plugged", {
--Colorschemes
  'projekt0n/github-nvim-theme',
  'rktjmp/lush.nvim',
  'ellisonleao/gruvbox.nvim',
  'Mofiqul/vscode.nvim',
-- Icons
  'kyazdani42/nvim-web-devicons',
--Telescope dependencies
  'nvim-lua/popup.nvim',
  'nvim-lua/plenary.nvim',
--Fuzzy file finder
  "nvim-telescope/telescope.nvim",
--View undo history
  'mbbill/undotree',
--Wrapper around git worktree operations, create, switch, and delete
 'ThePrimeagen/git-worktree.nvim',
--Automatically close X/HTML tags
 'alvan/vim-closetag',
--Git power tool
 'tpope/vim-fugitive',
--Surround tool
 'blackcauldron7/surround.nvim',
--Auto-completion
 'hrsh7th/nvim-compe',
 'glepnir/lspsaga.nvim',
--Snippets
 'norcalli/snippets.nvim',
--LSP client
'neovim/nvim-lspconfig',
'kabouzeid/nvim-lspinstall',
--Syntax highlighting
  {"nvim-treesitter/nvim-treesitter", ["do"] = ":TSUpdate"},
 'nvim-treesitter/nvim-treesitter-textobjects',
 'mfussenegger/nvim-dap',
--Language specific tools
 'jose-elias-alvarez/nvim-lsp-ts-utils',
})

----[FAST SETUPS]----
--surround
require"surround".setup{mappings_style = "surround"}
--git-worktree
local Worktree = require("git-worktree")
local Job = require("plenary.job")
local Path = require("plenary.path")

local function is_nrdp()
    return not not (string.find(vim.loop.cwd(), vim.env.NRDP, 1, true))
end

local function is_tvui()
    return not not (string.find(vim.loop.cwd(), vim.env.TVUI, 1, true))
end

local function get_nrdp_build_paths(path)
    return
        Path:new({vim.env.NRDP, path, "configure"}):absolute(),
        Path:new({vim.env.NRDP, "build", path}):absolute()
end

Worktree.on_tree_change(function(op, path, upstream)

    if op == Worktree.Operations.Switch and is_tvui() then
        Job:new({
            "./tvui", "install"
        }):start()
    end

    if op == Worktree.Operations.Create and is_nrdp() then
        local submodule = Job:new({
            "git", "submodule", "update"
        })

        local configure_path, build_path = get_nrdp_build_paths(path)
        local make_build = Job:new({
            "mkdir", "-p", build_path
        })

        local configure = Job:new({
            configure_path, "--ninja", "--debug",
            cwd = build_path,
        })

        submodule:and_then_on_success(make_build)
        make_build:and_then_on_success(configure)
        submodule:start()
    end
end)
