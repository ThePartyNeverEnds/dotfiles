local map=vim.api.nvim_set_keymap
local nor={noremap=true, silent=true}
-- Esc into normal mode faster
map("i","kj","<Esc>",nor)
map("i","jk","<Esc>",nor)
map("i","<C-s>","<Esc>:w<CR>",nor)
map("n","<C-s>",":w<CR>",nor)
-- Manage plugins faster
map("n","<LEADER>1",":source %<CR>",nor)
map("n","<LEADER>2",":PlugUpdate<CR>",nor)
map("n","<LEADER>3",":PlugInstall<CR>",nor)
map("n","<LEADER>4",":PlugClean<CR>",nor)
-- Indent visual mode
map("v","<","<gv",nor)
map("v",">",">gv",nor)
-- Move cursor faster
map("n","<DOWN>","8j",nor)
map("v","<DOWN>","8j",nor)
map("n","<UP>","8k",nor)
map("v","<UP>","8k",nor)
-- Switch between splits intuitively
map("n","<LEADER>h",":wincmd h<CR>",nor)
map("n","<LEADER>j",":wincmd j<CR>",nor)
map("n","<LEADER>k",":wincmd k<CR>",nor)
map("n","<LEADER>l",":wincmd l<CR>",nor)
-- Switch between buffers
map("","<C-l>",":bnext<CR>",nor)
map("","<C-h>",":bprev<CR>",nor)
-- Save/quit files faster
map("n","<LEADER>q",":q<CR>",nor)
map("n","<LEADER>w",":w<CR>",nor)
map("n","<LEADER>x",":x<CR>",nor)
-- Show timestamped undo history
map("n","<LEADER>u",":UndotreeShow<CR>",nor)
-- Paste smoothly
map("v","<LEADER>p",'"_dP',nor)
-- Yank into clipboard
map("n","<LEADER>y",'"+y',nor)
map("v","<LEADER>y",'"+y',nor)
map("n","<LEADER>Y",'gg"+yG',nor)
-- Delete elegantly
map("n","<LEADER>d",'"_d',nor)
map("v","<LEADER>d",'"_d',nor)
-- Drag lines up & down
map("v","J",":m '>+1<CR>gv=gv",nor)
map("v","K",":m '<-2<CR>gv=gv",nor)
----[ LSP KEYMAPS ]----
map("n","<SILENT>gd",":Lspsaga preview_definition<CR>",nor)
map("n","<SILENT><C-]>",[[<CMD>lua vim.lsp.buf.definition()<CR>]],nor)
map("n","<SILENT>gD",[[<CMD>lua vim.lsp.buf.declaration()<CR>]],nor)
map("n","<SILENT>gr",":Lspsaga rename<CR>",nor)
map("n","<SILENT>gi",[[<CMD>lua vim.lsp.buf.implementation()<CR>]],nor)
map("n","<SILENT>gf",[[<CMD>lua vim.lsp.buf.formatting()<CR>]],nor)
map("n","<SILENT>gn",[[<CMD>lua vim.lsp.buf.rename()<CR>]],nor)
map("n","<SILENT>gs",":Lspsaga signature_help<CR>",nor)
map("n","<SILENT>qa",":Lspsaga code_action<CR>",nor)
map("v","<SILENT>qa",":<C-U>Lspsaga range_code_action<CR>",nor)
map("n","<SILENT>K",":Lspsaga hover_doc<CR>",nor)
map("n","<SILENT><C-k>",[[<CMD>lua vim.lsp.buf.signature_help()<CR>]],nor)
map("n","<SILENT><C-p>",":Lspsaga diagnostic_jump_prev<CR>",nor)
map("n","<SILENT><C-n>",":Lspsaga diagnostic_jump_next<CR>",nor)
map("n","<SILENT>`t",":Lspsaga open_floaterm<CR>",nor)
map("t","<SILENT>`t",[[<C-\><C-n>:Lspsaga close_floaterm<CR>]],nor)
----[ PLUGIN KEYMAPS ]----
map("n","<LEADER>pp",[[<CMD>lua require'telescope.builtin'.builtin{}<CR>]],nor)
map("n","<LEADER>ff",":Telescope find_files<CR>",nor)
map("n","<LEADER>fs",":Telescope live_grep<CR>",nor)
map("n","<LEADER>fg",":Telescope git_files<CR>",nor)
map("n","<LEADER>fo",[[<CMD>lua require('telescope.builtin').oldfiles()<CR>]],nor)
map("n","<LEADER>fh",[[<CMD>lua require('telescope.builtin').help_tags()<CR>]],nor)
--map("n","<LEADER>fd",[[<CMD>lua require('b.telescope').search_dotfiles()<CR>]],nor)
--map("n","<LEADER>df",[[<CMD>lua require('b.telescope').search_dotfiles()<CR>]],nor)
map("n","<LEADER>'",[[<CMD>lua require'telescope.builtin'.marks{}<CR>]],nor)
map("n","<LEADER>/",[[<CMD>lua require'telescope.builtin'.current_buffer_fuzzy_find{}<CR>]],nor)
map("n","<LEADER>cs",[[<CMD>lua require'telescope.builtin'.colorscheme{}<CR>]],nor)
map("n",";",[[<CMD>lua require('telescope.builtin').buffers()<CR>]],nor)
