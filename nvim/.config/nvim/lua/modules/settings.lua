local opt=vim.opt
local cmd=vim.cmd
local g=vim.g
-- SETTINGS
g.mapleader=","
opt.mouse="a"
opt.wrap=false
opt.hidden=true
opt.showmode=false
opt.showmatch=false
opt.updatetime=20
opt.timeoutlen=500
opt.tabstop=2
opt.shiftwidth=2
opt.softtabstop=2
opt.expandtab=true
opt.nu=true
opt.relativenumber=true
opt.signcolumn="number"
opt.hlsearch=false
opt.incsearch=true
opt.cursorline=false
opt.laststatus=0
opt.backup=false
opt.undofile=true
opt.completeopt="menuone,noselect"
opt.scrolloff=8
opt.pumheight=10
opt.cmdheight=1
opt.termguicolors=true
-- COLORS
g.vscode_style = "dark"
cmd[[hi ColorColumn guibg=none]]
cmd[[hi TelescopeMatching guifg=orange]]
cmd[[colorscheme vscode]]
-- FILE TREE
g.netrw_altv=1
g.netrw_banner=0
g.netrw_winsize=25
g.netrw_liststyle=3
g.netrw_browse_split=4
g.netrw_localrmdir="rm -r"
