-- LSP setup
-- Set Default Prefix.
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics, {
    signs = true,
    underline = true,
    update_in_insert = false,
    virtual_text = {
        --prefix = "*",
        spacing = 0,
    },
  }
)
-- Define diagnostic signs
local signs = { Error = "X", Warning = "!", Hint = "?", Information = "I" }
for type, icon in pairs(signs) do
  local hl = "LspDiagnosticsSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
end

local function documentHighlight(client, _)
    -- Set autocommands conditional on server_capabilities
    if client.resolved_capabilities.document_highlight then
        vim.api.nvim_exec(
[[
hi LspReferenceRead cterm=bold ctermbg=red guibg=#464646
hi LspReferenceText cterm=bold ctermbg=red guibg=#464646
hi LspReferenceWrite cterm=bold ctermbg=red guibg=#464646
augroup lsp_document_highlight
  autocmd! * <buffer>
  autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
  autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
augroup END
]],
            false
        )
    end
end
-- Setup servers
local lsp_config = {}

function lsp_config.common_on_attach(client, bufnr)
    documentHighlight(client, bufnr)
end

function lsp_config.tsserver_on_attach(client, bufnr)
    lsp_config.common_on_attach(client, bufnr)
    client.resolved_capabilities.document_formatting = false
end

local function setup_servers()
  require'lspinstall'.setup()
  local servers = require'lspinstall'.installed_servers()
  local lspconfig = require'lspconfig'
  for _, server in pairs(servers) do
    lspconfig[server].setup{}
  end
  lspconfig.lua.setup{ -- Removes "undefined global `vim` warning"
    settings = {
      Lua = {
        diagnostics = {
          globals = { 'vim' }
        }
      }
    }
  }
end

setup_servers()

-- Automatically reload after `:LspInstall <server>` so you we't have to restart neovim
require'lspinstall'.post_install_hook = function ()
  setup_servers() -- reload installed servers
  vim.cmd("bufdo e") -- this triggers the FileType autocmd that starts the server
end
